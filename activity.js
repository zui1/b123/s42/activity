let inputFirstName = document.querySelector("#txt-first-name");

let fullNameDisplay = document.querySelector("#full-name-display");

let inputLastName = document.querySelector("#txt-last-name");

let submitLabel = document.querySelector("#submit-btn");

const showName = () => {

	console.log(inputFirstName.value);
	console.log(inputLastName.value);
	fullNameDisplay.innerHTML = `${inputFirstName.value } ${inputLastName.value}`

}

inputFirstName.addEventListener('keyup', showName);

inputLastName.addEventListener('keyup', showName);


submitLabel.addEventListener('click', () => {

	if(inputFirstName.value.length === 0 || inputLastName.value.length ===0){

		alert("Please input First Name and Last Name")
	} else {

		alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}.`)

	}

})